module gitlab.com/mironet/promql-rewriter

go 1.15

require (
	github.com/gorilla/handlers v1.5.1
	github.com/sirupsen/logrus v1.2.0
	github.com/spf13/cobra v1.1.2
)
