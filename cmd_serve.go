package main

import (
	"fmt"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"syscall"

	"github.com/gorilla/handlers"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

func newServe(pather CommandPather) *cobra.Command {
	var flags struct {
		address             string
		labelName           string
		upstream            string
		fromHTTPHeader      string
		fromJWTClaim        string
		fromJWTinHTTPHeader string
		loglevel            string
		staticValue         string
		insecureSkipVerify  bool
		removeTokenHeader   bool
		// TODO: Handle CORS.
	}

	var parseOpts = func() ([]rewriteOpt, error) {
		logrus.Infof("using label name '%s' in upstream requests", flags.labelName)
		opts := []rewriteOpt{
			withLabelName(flags.labelName),
		}
		if flags.staticValue != "" {
			opts = append(opts, withStaticValue(flags.staticValue))
			logrus.Infof("using static value '%s'", flags.staticValue)
			return opts, nil
		}
		opts = append(opts, withHeaderName(flags.fromHTTPHeader))
		if flags.fromJWTClaim != "" {
			logrus.Infof("using JWT claim name '%s'", flags.fromJWTClaim)
			opts = append(opts, withParseJWT())
			opts = append(opts, withClaimName(flags.fromJWTClaim))
		}
		if flags.fromJWTinHTTPHeader != "" {
			logrus.Infof("using JWT token from HTTP header named '%s'", flags.fromJWTinHTTPHeader)
			opts = append(opts, withHeaderName(flags.fromJWTinHTTPHeader))
		}
		if flags.removeTokenHeader {
			logrus.Infof("removing JWT headers on upstream calls")
			opts = append(opts, withRemoveHeader(flags.removeTokenHeader))
		}
		return opts, nil
	}

	var cmd = &cobra.Command{
		Use:     "serve",
		Short:   "serve starts the rewriting server.",
		Example: fmt.Sprintf("  %[1]s serve --upstream http://localhost:9090/", pather.CommandPath()),
		RunE: func(cmd *cobra.Command, args []string) error {
			if flags.upstream == "" {
				return fmt.Errorf("--upstream should be set")
			}
			u, err := url.Parse(flags.upstream)
			if err != nil {
				return err
			}
			if u.Scheme != "https" && u.Scheme != "http" {
				return fmt.Errorf("scheme of %s is not http:// or https://", flags.upstream)
			}
			switch flags.loglevel {
			case "trace":
				logrus.SetLevel(logrus.TraceLevel)
			case "debug":
				logrus.SetLevel(logrus.DebugLevel)
			case "error":
				logrus.SetLevel(logrus.ErrorLevel)
			default:
				logrus.SetLevel(logrus.InfoLevel)
			}
			logrus.Infof("set log level to %s", logrus.GetLevel())
			// Add basic sanity checks, where the usage help message should be
			// printed on error, before this line. After this line, the usage
			// message is no longer printed on error.
			cmd.SilenceUsage = true

			// Configure proxy to upstream.
			opts, err := parseOpts()
			if err != nil {
				return err
			}
			rewrite, err := newRewriteEngine(opts...)
			if err != nil {
				return err
			}
			proxy, err := newProxy(flags.upstream, rewrite)
			if err != nil {
				return err
			}
			proxy = handlers.CombinedLoggingHandler(os.Stderr, proxy)
			srv, err := newServer(flags.address, proxy)
			if err != nil {
				return fmt.Errorf("error starting server: %w", err)
			}

			// Start listening for incoming requests.
			errc := make(chan error)
			go func() {
				logrus.Infof("starting server version %s", version)
				logrus.Infof("listening on %s", flags.address)
				logrus.Infof("upstream is %s", flags.upstream)
				errc <- srv.ListenAndServe()
			}()

			term := make(chan os.Signal, 1)
			signal.Notify(term, os.Interrupt, syscall.SIGTERM)

			select {
			case sig := <-term:
				logrus.Infof("received signal %s, terminating", sig)
				_ = srv.Close()
			case err := <-errc:
				if err != http.ErrServerClosed {
					logrus.Errorf("server stopped with error: %v", err)
					return err
				}
			}
			return nil
		},
	}

	cmd.Flags().StringVar(&flags.address, "address", ":7070", "Address where to listen on.")
	cmd.Flags().StringVar(&flags.labelName, "label", "tenant", "Label name we append to the query string sent to upstream.")
	cmd.Flags().StringVar(&flags.upstream, "upstream", "", "Upstream we send requests to (label-proxy).")
	cmd.Flags().StringVar(&flags.fromHTTPHeader, "from-http-header", "", "Copy label value from this HTTP header, case-insensitive.")
	cmd.Flags().StringVar(&flags.fromJWTClaim, "from-jwt-claim", "", "Copy label value from this JWT claim.")
	cmd.Flags().StringVar(&flags.fromJWTinHTTPHeader, "from-jwt-in-http-header", "", "Read JWT token from this HTTP header. Strips 'Bearer' if present.")
	cmd.Flags().StringVar(&flags.staticValue, "static", "", "Static value for the label value we append to upstream.")
	cmd.Flags().StringVar(&flags.loglevel, "loglevel", "", "Log level to use.")
	//cmd.Flags().BoolVar(&flags.insecureSkipVerify, "insecure-skip-verify", false, "Don't verify TLS on upstream ⚠️.")
	cmd.Flags().BoolVar(&flags.removeTokenHeader, "remove-token-header", false, "Remove the HTTP header we used to get the JWT from in calls to upstream server.")
	return cmd
}
