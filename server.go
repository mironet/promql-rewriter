package main

import (
	"net/http"
	"time"
)

func newServer(address string, routes http.Handler) (*http.Server, error) {
	mux := http.NewServeMux()
	mux.Handle("/", routes)

	srv := &http.Server{
		Handler:      mux,
		Addr:         address,
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
	}

	return srv, nil
}
