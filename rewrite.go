package main

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
	"strings"

	"github.com/sirupsen/logrus"
)

const (
	defaultJWTHeaderName = "Authorization"
	defaultClaimName     = "tenant"
)

type rewrite struct {
	labelName        string // The label we append to the query string.
	headerName       string // The HTTP header name we use for the label value.
	parseJWT         bool   // Try to parse the header name as a JWT (strip 'Bearer' if present). If headerName is set, we look for the JWT token there.
	claimName        string // JWT claim to look for if parseJWT = true.
	staticValue      string // The static value of the label name we append to the query string. Overrides anything else.
	removeUsedHeader bool   // Remove the header we used to get the JWT in calls to upstream server.
}

type director func(r *http.Request)

type rewriter interface {
	Rewrite(next director) director
}

// Rewrite can be used to chain this rewrite engine to a httputil.ReverseProxy.
func (re *rewrite) Rewrite(next director) director {
	return func(r *http.Request) {
		if err := r.ParseForm(); err != nil {
			logrus.Errorf("could not parse form data or query, not modifying request: %v", err)
			next(r)
			return
		}
		values, err := re.handle(r)
		if err != nil {
			logrus.Error(err)
			return // This will lead to a failed proxy request, not very nice though.
		}
		switch r.Method {
		case http.MethodGet:
			r.URL.RawQuery = values.Encode()
		case http.MethodPost:
			body := values.Encode()
			r.ContentLength = int64(len(body))
			r.Body = ioutil.NopCloser(strings.NewReader(values.Encode()))
		}
		if re.removeUsedHeader {
			hdr := r.Header
			hdr.Del(re.headerName)
			r.Header = hdr
		}
		// Dump the rewritten request in trace mode.
		if logrus.IsLevelEnabled(logrus.TraceLevel) {
			dump, err := httputil.DumpRequest(r, true)
			if err != nil {
				logrus.Errorf("error while dumping request: %v", err)
				return
			}
			logrus.Tracef("dumping rewritten request ...")
			if _, err := io.Copy(os.Stderr, bytes.NewBuffer(dump)); err != nil {
				logrus.Errorf("error while dumping request: %v", err)
				return
			}
		}
		logrus.Infof("rewrite: %s", r.URL.String())
		next(r)
	}
}

func (re *rewrite) handle(r *http.Request) (url.Values, error) {
	if re.staticValue != "" {
		return re.handleStaticValue(r.Form), nil
	}
	if re.parseJWT {
		return re.handleJWT(r)
	}
	if re.headerName != "" {
		return re.handleHeaderCopy(r)
	}
	// Should not happen.
	return nil, fmt.Errorf("did not rewrite anything, no valid instructions set")
}

func (re *rewrite) handleStaticValue(values url.Values) url.Values {
	values[re.labelName] = []string{re.staticValue}
	return values
}

func (re *rewrite) handleHeaderCopy(r *http.Request) (url.Values, error) {
	hdr := strings.TrimSpace(r.Header.Get(re.headerName))
	values := r.Form
	if values == nil {
		values = make(url.Values)
	}
	values[re.labelName] = []string{hdr}
	return values, nil
}

func (re *rewrite) handleJWT(r *http.Request) (url.Values, error) {
	token := r.Header.Get(re.headerName)
	token = strings.TrimPrefix(token, "Bearer")
	token = strings.TrimSpace(token)

	parts := strings.Split(token, ".")
	if len(parts) != 3 {
		return nil, fmt.Errorf("could not parse JWT token, should have at least 3 parts separated by a dot (.)")
	}

	decoded, err := decodeJWTSegment(parts[1])
	if err != nil {
		return nil, fmt.Errorf("could not decode JWT data segment: %w", err)
	}

	var claims map[string]interface{}
	if err := json.Unmarshal(decoded, &claims); err != nil {
		return nil, fmt.Errorf("could not decode JSON from JWT segment: %w", err)
	}

	val, ok := claims[re.claimName]
	if !ok {
		return nil, fmt.Errorf("could not find claim with name %s in JWT", re.claimName)
	}
	value, ok := val.(string)
	if !ok {
		return nil, fmt.Errorf("could not convert claim value of claim %s to string", re.claimName)
	}

	values := r.Form
	if values == nil {
		values = make(url.Values)
	}
	values[re.labelName] = []string{value}
	return values, nil
}

// decodeJWTSegment decodes JWT specific base64url encoding with padding stripped.
// Taken from: https://github.com/dgrijalva/jwt-go/blob/0987fb8fd48e32823701acdac19f5cfe47339de4/jwt.go#L219
func decodeJWTSegment(seg string) ([]byte, error) {
	if l := len(seg) % 4; l > 0 {
		seg += strings.Repeat("=", 4-l)
	}
	return base64.URLEncoding.DecodeString(seg)
}

type rewriteOpt func(*rewrite) error

func withLabelName(name string) rewriteOpt {
	return func(r *rewrite) error {
		r.labelName = name
		return nil
	}
}

func withHeaderName(name string) rewriteOpt {
	return func(r *rewrite) error {
		r.headerName = name
		return nil
	}
}

func withParseJWT() rewriteOpt {
	return func(r *rewrite) error {
		r.parseJWT = true
		return nil
	}
}

func withClaimName(name string) rewriteOpt {
	return func(r *rewrite) error {
		r.claimName = name
		return nil
	}
}

func withStaticValue(value string) rewriteOpt {
	return func(r *rewrite) error {
		r.staticValue = value
		return nil
	}
}

func withRemoveHeader(remove bool) rewriteOpt {
	return func(r *rewrite) error {
		r.removeUsedHeader = remove
		return nil
	}
}

func newRewriteEngine(opts ...rewriteOpt) (*rewrite, error) {
	re := &rewrite{}

	for _, opt := range opts {
		if err := opt(re); err != nil {
			return nil, err
		}
	}

	// Do a bit of sanity checking.
	if re.labelName == "" {
		if re.staticValue != "" {
			return nil, fmt.Errorf("no label name set")
		}
		if re.headerName == "" {
			if re.parseJWT == false {
				return nil, fmt.Errorf("either label name, HTTP header name or JWT parsing should be set")
			}
		}
	}
	if re.parseJWT {
		if re.headerName == "" {
			re.headerName = defaultJWTHeaderName
		}
		if re.claimName == "" {
			re.claimName = defaultClaimName
		}
	}

	return re, nil
}
