# PromQL Rewriter

This program acts as an HTTP proxy for PromQL queries to the well-known API endpoints of [Prometheus](https://prometheus.io/). It is supposed to sit between the [prom-label-proxy](https://github.com/prometheus-community/prom-label-proxy) and an authenticator proxy, like e.g. [oauth2-proxy](https://github.com/oauth2-proxy/oauth2-proxy) and rewrites HTTP requests to the label proxy based on incoming headers and/or tokens.

![Diagram of where the rewriter sits.](docs/diagram.png)

## Mode of Operation

The incoming request is being rewritten so it contains the label we want to enforce. The label name is configured when starting the server. The value is read from either a plain HTTP header or as a JWT token whose source is either a specific HTTP header or the default `Authorization` header.

**To be clear:** This server does not in any way validate the JWT. It will just parse the middle part of the token and use the named claim in there. Authentication and authorization should be handled before calling this server.

## Example

Assuming we have a Prometheus [label proxy enforcer](https://github.com/prometheus-community/prom-label-proxy) listening on `localhost:9091` and we expect valid JWT requests coming in and we want to look for the claim `tenant`, we start this server like so:

```bash
./promql-rewriter serve --upstream http://localhost:9091/ --from-jwt-claim tenant
```

This will change the incoming request so it contains the `tenant` form/query value. An example for a rewritten POST request (form-data):

```text
POST /api/v1/query HTTP/1.1
Host: localhost:7070
Accept: */*
Authorization: Bearer eyJhbGciOiJSUzI1NiIs...
Content-Length: 8
Content-Type: application/x-www-form-urlencoded
User-Agent: curl/7.58.0

query=up&tenant=mysupertenant
```

**Note:** The request above has been shortened for readability. The JWT contains the claim:

```json
{
    "tenant": "mysupertenant"
}
```

This request is then forwarded to the upstream label proxy on `localhost:9091`.
It could also have been a GET request where the URL query string would have been changed instead of form-data in the body.

```text
GET /api/v1/query?query=up HTTP/1.1
Host: localhost:7070
Accept: */*
Authorization: Bearer eyJhbGciOiJSUzI1NiIs...
User-Agent: curl/7.58.0

INFO[0018] rewrite: /api/v1/query?query=up&tenant=mysupertenant
```
