package main

import "net/http"

func chainHandlers(handlers ...http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		for _, h := range handlers {
			h.ServeHTTP(rw, r)
		}
	})
}
