package main

import (
	"bytes"
	"io"
	"log"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"

	"github.com/sirupsen/logrus"
)

func newProxy(upstream string, re rewriter) (http.Handler, error) {
	remote, err := url.Parse(upstream)
	if err != nil {
		return nil, err
	}

	errlog := logrus.StandardLogger().WriterLevel(logrus.ErrorLevel)
	proxy := httputil.NewSingleHostReverseProxy(remote)
	proxy.Director = re.Rewrite(proxy.Director)
	proxy.ModifyResponse = func(r *http.Response) error {
		r.Header.Del("Server")
		r.Header.Del("X-Powered-By")
		return nil
	}
	proxy.ErrorLog = log.New(errlog, "proxy: ", 0)
	proxy.ErrorHandler = func(w http.ResponseWriter, r *http.Request, err error) {
		logrus.Error(err)
		http.Error(w, "", http.StatusBadGateway)
	}

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Method != http.MethodGet && r.Method != http.MethodPost {
			http.Error(w, "", http.StatusMethodNotAllowed)
		}
		if logrus.IsLevelEnabled(logrus.TraceLevel) {
			dump, err := httputil.DumpRequest(r, true)
			if err != nil {
				logrus.Errorf("error dumping incoming request: %v", err)
				http.Error(w, "", http.StatusInternalServerError)
				return
			}
			if _, err := io.Copy(os.Stderr, bytes.NewBuffer(dump)); err != nil {
				logrus.Errorf("error dumping incoming request: %v", err)
				http.Error(w, "", http.StatusInternalServerError)
				return
			}
		}
		proxy.ServeHTTP(w, r)
	}), nil
}
