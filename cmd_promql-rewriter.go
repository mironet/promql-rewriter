package main

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

// CommandPather returns the path to a command.
type CommandPather interface {
	CommandPath() string
}

func main() {
	cmd := &cobra.Command{
		Use:           "promql-rewriter",
		Short:         "promql-rewriter rewrites promql queries so prom-label-proxy can enforce them.",
		SilenceErrors: true,
	}
	cmd.AddCommand(
		newCompletion(cmd),
		newVersion(cmd),
		newServe(cmd),
	)
	if err := cmd.Execute(); err != nil {
		fmt.Fprintf(os.Stderr, "Error: %s\n", err)
		os.Exit(1)
	}
}
