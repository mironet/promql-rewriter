package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"reflect"
	"strings"
	"testing"
)

func fixture(filename string, t *testing.T) []byte {
	data, err := ioutil.ReadFile(fmt.Sprintf("testdata/fixtures/%s", filename))
	if err != nil {
		t.Fatal(err)
		return nil
	}
	return data
}

func fixtureString(filename string, t *testing.T) string {
	return string(fixture(filename, t))
}

func Test_rewrite_handleJWT(t *testing.T) {
	const (
		customHeaderName   = "X-Forwarded-Access-Token"
		verbatimHeaderName = "X-Tenant"
	)

	getWithJWT, err := http.NewRequest(http.MethodGet, "/query", nil)
	if err != nil {
		t.Fatal(err)
	}
	getWithJWT.Header.Add(defaultJWTHeaderName, strings.TrimSpace(fixtureString("jwt.txt", t)))

	getWithJWTCustom, err := http.NewRequest(http.MethodGet, "/query", nil)
	if err != nil {
		t.Fatal(err)
	}
	getWithJWTCustom.Header.Add(customHeaderName, strings.TrimSpace(fixtureString("jwt.txt", t)))

	values := make(url.Values)
	values.Add("query", "up")
	postWithJWT, err := http.NewRequest(http.MethodPost, "/query", strings.NewReader(values.Encode()))
	if err != nil {
		t.Fatal(err)
	}
	postWithJWT.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	postWithJWT.Header.Add(defaultJWTHeaderName, strings.TrimSpace(fixtureString("jwt.txt", t)))

	type fields struct {
		labelName   string
		headerName  string
		parseJWT    bool
		claimName   string
		staticValue string
	}
	type args struct {
		r *http.Request
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    url.Values
		wantErr bool
	}{
		{
			name: "default header JWT",
			fields: fields{
				labelName:  "tenant",
				headerName: defaultJWTHeaderName,
				parseJWT:   true,
				claimName:  "tenant",
			},
			args: args{
				r: getWithJWT,
			},
			want: url.Values{
				"tenant": []string{"mytenant"},
			},
		},
		{
			name: "POST with default header JWT",
			fields: fields{
				labelName:  "tenant",
				headerName: defaultJWTHeaderName,
				parseJWT:   true,
				claimName:  "tenant",
			},
			args: args{
				r: postWithJWT,
			},
			want: url.Values{
				"tenant": []string{"mytenant"},
			},
		},
		{
			name: "custom header JWT",
			fields: fields{
				labelName:  "tenant",
				headerName: customHeaderName,
				parseJWT:   true,
				claimName:  "tenant",
			},
			args: args{
				r: getWithJWTCustom,
			},
			want: url.Values{
				"tenant": []string{"mytenant"},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			re := &rewrite{
				labelName:   tt.fields.labelName,
				headerName:  tt.fields.headerName,
				parseJWT:    tt.fields.parseJWT,
				claimName:   tt.fields.claimName,
				staticValue: tt.fields.staticValue,
			}
			got, err := re.handleJWT(tt.args.r)
			if (err != nil) != tt.wantErr {
				t.Errorf("rewrite.handleJWT() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("rewrite.handleJWT() = %v, want %v", got, tt.want)
			}
		})
	}
}
