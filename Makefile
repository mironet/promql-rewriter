IMAGE_NAME=registry.gitlab.com/mironet/promql-rewriter
SERVER_BIN=promql-rewriter
PKG_LIST=`go list ./... | grep -v /vendor/`
PROGNAME=promql-rewriter

# HELP
# This will output the help for each task
# thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: help

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.DEFAULT_GOAL := help

build: ## Build go binary.
	go generate
	go build -ldflags "-X main.version=$$(git describe --always)" -o $(PROGNAME)

build-race: ## Build go binary with race detector on.
	go generate
	go build -race -ldflags "-X main.version=$$(git describe --always)" -o $(PROGNAME)

build-docker: export DOCKER_BUILDKIT = 1
build-docker: ## Build docker images.
	docker build --build-arg APP_VERSION=$$(git describe --always) -t $(IMAGE_NAME) -f Dockerfile .

test: ## Execute all tests.
	go test -race $(PKG_LIST) -count 1
